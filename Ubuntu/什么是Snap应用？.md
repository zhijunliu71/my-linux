# 什么是Snap应用？

> https://cn.ubuntu.com/blog/what-is-snap-application

如果你在使用Ubuntu 18.04/20.04 LTS版本的Ubuntu系统，会发现系统里面多了一个应用格式包——.snap包。Snap包是Ubuntu 16.04 LTS发布时引入的新应用格式包。目前已流行在Ubuntu且在其他如Debian、Arch Linux、Fedora、Kaili Linux、openSUSE、Red Hat等Linux发行版上通过snapd来安装使用snap应用。较传统Linux的rpm，deb软件包，snap有什么特点和优势呢？下面将为你介绍snap软件包。

## 安全，通用的Linux软件包

当你在安装完snap后，你会发现在在根目录下会出现如/dev/loop0的挂载点，这些挂载点正是snap软件包的目录。Snap使用了[squashFS](https://en.wikipedia.org/wiki/SquashFS)文件系统，一种开源的压缩，只读文件系统，基于GPL协议发行。一旦snap被安装后，其就有一个只读的文件系统和一个可写入的区域。应用自身的执行文件、库、依赖包都被放在这个只读目录，意味着该目录不能被随意篡改和写入。

squashFS文件系统的引入，使得snap的安全性要优于传统的Linux软件包。同时，每个snap默认都被严格限制（confined），即限制系统权限和资源访问。但是，可通过授予权限策略来获得对系统资源的访问。这也是安全性更好的表现。

![img](https://res.cloudinary.com/canonical/image/fetch/f_auto,q_auto,fl_sanitize,c_fill,w_720/https://ubuntu.com/wp-content/uploads/7245/Snipaste_2020-10-21_12-37-29.jpg)

Snap可包含一个或多个服务，支持cli（命令行）应用，GUI图形应用以及无单进程限制。因此，你可以单个snap下调用一个或多个服务。对于某些多服务的应用来说，非常方便。前面说到snap间相互隔离，那么怎么交换资源呢？答案是可以通过interface（接口）定义来做资源交换。interface被用于让snap可访问OpenGL加速，声卡播放、录制，网络和HOME目录。Interface由slot和plug组成即提供者和消费者。

## 如何使用snap软件包
通过上述简单介绍，大概了解了什么是snap应用包，如你已经在电脑上安装了Ubuntu，那么就可以通过下面的操作来安装使用snap软件包。Snap的基本命令：install,remove,find,list,infor,refresh等等。

1.安装snap，可使用以下命令或图形界面的store通过鼠标点击操作：

```
sudo snap install code //安装code snap
```

2.卸载snap

```
sudo snap remove code
```

3.搜索snap

```
snap find code
```

4.查看snap信息

```
snap info code
```

5.查看已安装的snap

```
snap list
```

6.更新snap

```
sudo snap refresh code channel=latest/stable //channel来指定通道版本
```

更多关于snap的使用说明，请访问snap网站[文档](https://snapcraft.io/docs)。

## 不止Linux桌面应用 

Snap除了在Ubuntu 桌面和其他Linux发行版桌面系统上使用外，还能在Ubuntu server和Ubuntu Core上使用且为Ubuntu Core默认应用格式包，Ubuntu Core是迷你，与Ubuntu一致，专为物联网设备、嵌入式平台设计，更多内容请访问[Ubuntu Core网页](https://ubuntu.com/core)。

目前，Ubuntu的相关产品已以snap包的形式发布，例如Ubuntu [MAAS](https://maas.io/)，[Juju](https://jaas.ai/)，[Multipass](https://multipass.run/)，[MicroK8s](http://microk8s.io/)，[MicroStack](https://microstack.run/)等等。借助snap，你可以一键安装专为笔记本工作站打造的Kubernetes和OpenStack，省去了安装等待和繁琐配置过程。对于开发和测试团队来说无疑是一个更高效的方案，将更多的精力和资源投入到关键价值上。

## Snap应用开发与Snapcraft

介绍了这么多snap相关内容，如何snap如何开发呢？Snap应用使用snapcraft来开发，并且snapcraft已打包为snap应用，一键安装：sudo snap install snapcraft –classic 即可开始构建你的snap应用。关于snap开发和snapcraft使用的更多内容，请加入本月[Ubuntu Tech Live视频直播](https://cn.ubuntu.com/engage/ubuntu-tech-live-episode-3)，届时Ubuntu 专家将现场教学，开发自己的第一个snap应用。