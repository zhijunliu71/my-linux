### ubuntu/lubuntu/xbuntu的关系

- `ubuntu`是标准版，VMWare可以自动安装系统和VMTool；

- `lubuntu`是`lightweight ubuntu`，轻量化，但是界面太简陋，很多操作都不方便，VMWare不会自动安装系统和VMTool；

- `xubuntu`是使用`xfce`框架的`ubuntu`，也很轻量化，有开始菜单，最接近windows界面，右键可以直接在任何地方打开终端，不用再cd到某个目录了，比其他版本方便多了，VMWare也可以自动安装系统和VMTool，虚拟机安装首推版本。