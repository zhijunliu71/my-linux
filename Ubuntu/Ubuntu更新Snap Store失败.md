# Ubuntu更新Snap Store失败

这个问题出现的原因很简单：Snap Store无法自己更新自己

很容易理解，举个不恰当的例子，某个手机应用更新时，是无法运行这个应用的

解决办法也很简单，既然他自己不能更新他自己，那就使用命令行帮他更新

1. 直接命令行输入 

   ```
   sudo snap refresh snap-store
   ```

2. 如果出现下面报错：

   这个是因为当前Snap Store正在运行，可以看到，当前的进程号是 1701 ，只需要 在终端输入 `kill 1701` 就好啦

3. 然后再执行 

   ```
   sudo snap refresh snap-store
   ```

更新成功啦