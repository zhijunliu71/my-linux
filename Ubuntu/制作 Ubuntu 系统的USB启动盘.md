# 制作 Ubuntu 系统的USB启动盘

> https://blog.csdn.net/u013553529/article/details/78307520

在 Windows 中制作 Ubuntu 系统的USB启动盘

## 1. 下载ubuntu iso
目前稳定的长期支持的版本是 Ubuntu 16.04.3 LTS 。

下载地址：
官网：https://www.ubuntu.com/download/desktop
镜像：http://mirrors.163.com/ubuntu-releases/16.04.3/

## 2. 下载 Rufus
Rufus 是Ubuntu官网推荐的USB启动盘制作工具，此工具的特点：
* 体积小，只有几百KB （Rufus 2.17版只有945KB）
* 功能全
* 免安装，下载后可以直接使用
* 免费
* 开源

Rufus 的官网： https://rufus.akeo.ie/
目前Rufus的最新版本是2.17，下载地址：https://rufus.akeo.ie/downloads/rufus-2.17.exe

## 3. **制作启动盘**

- （1）打开Rufus，插上U盘。

  ![这里写图片描述](https://img-blog.csdn.net/20171027220426805?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQvdTAxMzU1MzUyOQ==/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/SouthEast)

  | 设置项                 | 说明                                                         |
  | ---------------------- | ------------------------------------------------------------ |
  | 设备                   | 选择你的U盘，为了避免选错，只插一个U盘                       |
  | 分区方案和目标系统类型 | 如果你的主板是UEFI的，则选带UEFI的那个；如果主板是BIOS的，则选兼容BIOS的 |
  | 文件系统               | 默认FAT32即可                                                |
  | 簇大小                 | 默认即可                                                     |
  | 新卷标                 | 设置U盘的名称，这一项在选择Ubuntu的iso文件之后会自动修改     |

- （2）选择Ubuntu的iso文件
  点击“创建一个启动盘使用”那一行后面的光盘图标，选择iso文件。

- （3）点击“开始”
  **确保U盘中的文件备份了。**
  遇到提示框，基本上点击OK，大概4分多钟就可以将iso写到U盘了。

## 4. 安装Ubuntu
Ubuntu官网也提供了Ubuntu的安装教程。
在你想要安装Ubuntu的主机上，插上U盘，启动电脑，进入BIOS或者UEFI。
需要注意的是，如何进入BIOS，在开机界面中已经给提示了。例如，按F2键进入Setup。

## 参考
如何在ubuntu/windows/macOS中烧录ubuntu系统的DVD盘或者USB启动盘，请参考ubuntu官网： https://www.ubuntu.com/download/desktop