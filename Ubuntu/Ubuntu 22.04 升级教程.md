# Ubuntu 22.04 升级教程

> https://www.bandwagonhost.net/12638.html

**从 Ubuntu 20.04 升级到 Ubuntu 22.04 方法：**  
　　一般不建议生产环境下直接这么升级，可能会出现问题。本文介绍 Ubuntu 22.04 升级教程，主要是从 Ubuntu 20.04 升级到 Ubuntu 22.04 的教程。如果是更老的 Ubuntu 版本，可能需要先升级到 Ubuntu 20.04 或者是建议备份一下建站环境和数据之后直接安装 Ubuntu 22.04 吧。

## 一、Ubuntu 22.04 升级前准备
除非你是物理服务器，以及没有用过奇奇怪怪定制或修改的内核的 KVM 构架的 VPS 和云主机，否则升级大版本更新内核是有一定机率导致 Grub 加载失败的，切记备份重要数据！

以下操作需要在 root 用户下完成，请使用 sudo -i 或 su root 切换到 root 用户进行操作。

- 首先升级系统到最新状态：

  ```bash
  sudo apt update
  sudo apt upgrade -y
  sudo apt dist-upgrade -y
  sudo apt autoclean
  sudo apt autoremove -y
  ```

升级之后需要进行重启，建议重启让最新的内核生效。

## 二、Ubuntu 22.04 升级方法一
第一种方法是使用 do-release-upgrade 命令，这个方法比较简单，一般来说可以首选这个方法。

- 首先安装 update-manager-core 软件包：

  ```bash
  sudo apt install update-manager-core
  ```

- 然后运行 do-release-upgrade -d 即可更新。按照提示操作即可。

  ```bash
  sudo do-release-upgrade -d
  ```

## 三、Ubuntu 22.04 升级方法二
还有一种就是手工修改 apt 源文件：

- 首先更新 apt 源，替换 focal 为 jammy：

  ```bash
  sed -i 's/focal/jammy/g' /etc/apt/sources.list
  sed -i 's/focal/jammy/g' /etc/apt/sources.list.d/*.list
  ```

- 默认的系统 apt 源文件 /etc/apt/sources.list 应该是类似这样的：

  ```
  # See http://help.ubuntu.com/community/UpgradeNotes for how to upgrade to
  # newer versions of the distribution.
  deb http://archive.ubuntu.com/ubuntu/ jammy main restricted
  # deb-src http://archive.ubuntu.com/ubuntu/ jammy main restricted
  
  ## Major bug fix updates produced after the final release of the
  ## distribution.
  deb http://archive.ubuntu.com/ubuntu/ jammy-updates main restricted
  # deb-src http://archive.ubuntu.com/ubuntu/ jammy-updates main restricted
  
  ## N.B. software from this repository is ENTIRELY UNSUPPORTED by the Ubuntu
  ## team. Also, please note that software in universe WILL NOT receive any
  ## review or updates from the Ubuntu security team.
  deb http://archive.ubuntu.com/ubuntu/ jammy universe
  # deb-src http://archive.ubuntu.com/ubuntu/ jammy universe
  deb http://archive.ubuntu.com/ubuntu/ jammy-updates universe
  # deb-src http://archive.ubuntu.com/ubuntu/ jammy-updates universe
  
  ## N.B. software from this repository is ENTIRELY UNSUPPORTED by the Ubuntu
  ## team, and may not be under a free licence. Please satisfy yourself as to
  ## your rights to use the software. Also, please note that software in
  ## multiverse WILL NOT receive any review or updates from the Ubuntu
  ## security team.
  deb http://archive.ubuntu.com/ubuntu/ jammy multiverse
  # deb-src http://archive.ubuntu.com/ubuntu/ jammy multiverse
  deb http://archive.ubuntu.com/ubuntu/ jammy-updates multiverse
  # deb-src http://archive.ubuntu.com/ubuntu/ jammy-updates multiverse
  
  ## N.B. software from this repository may not have been tested as
  ## extensively as that contained in the main release, although it includes
  ## newer versions of some applications which may provide useful features.
  ## Also, please note that software in backports WILL NOT receive any review
  ## or updates from the Ubuntu security team.
  deb http://archive.ubuntu.com/ubuntu/ jammy-backports main restricted universe multiverse
  # deb-src http://archive.ubuntu.com/ubuntu/ jammy-backports main restricted universe multiverse
  
  ## Uncomment the following two lines to add software from Canonical's
  ## 'partner' repository.
  ## This software is not part of Ubuntu, but is offered by Canonical and the
  ## respective vendors as a service to Ubuntu users.
  # deb http://archive.canonical.com/ubuntu jammy partner
  # deb-src http://archive.canonical.com/ubuntu jammy partner
  
  deb http://archive.ubuntu.com/ubuntu jammy-security main restricted
  # deb-src http://archive.ubuntu.com/ubuntu jammy-security main restricted
  deb http://archive.ubuntu.com/ubuntu jammy-security universe
  # deb-src http://archive.ubuntu.com/ubuntu jammy-security universe
  deb http://archive.ubuntu.com/ubuntu jammy-security multiverse
  # deb-src http://archive.ubuntu.com/ubuntu jammy-security multiverse
  ```

国内服务器可以替换 `archive.ubuntu.com` 为 `mirrors.tuna.tsinghua.edu.cn`

- 然后我们再次执行更新系统：

  ```bash
  sudo apt update
  sudo apt upgrade -y
  sudo apt dist-upgrade -y
  ```

  *更新过程种会提示一些软件是否需要自动重启，选 Yes 即可，以及一些软件的配置文件是否需要更新，按照自己的情况选择即可，默认回车即视为使用旧的配置文件，一般会出现在 OpenSSH 等软件的更新上。*

- 更新后删除不必要的软件和依赖：

  ```bash
  sudo apt autoclean
  sudo apt autoremove -y
  ```

- 然后使用 reboot 命令重启系统，耐心等待后，查看最新的系统版本：

  ```bash
  root@ubuntu ~ # lsb_release -a
  No LSB modules are available.
  Distributor ID: Ubuntu
  Description:    Ubuntu 22.04 LTS
  Release:        22.04
  Codename:       jammy
  ```

  ```bash
  root@ubuntu ~ # uname -a
  Linux ubuntu 5.15.0-25 #4-Ubuntu SMP Fri Apr 1 07:36:38 UTC 2022 x86_64 x86_64 x86_64 GNU/Linux
  ```

这时我们就已经更新到了最新的 Ubuntu 22.04 Jammy 和内核了。

参考：https://u.sb/ubuntu-upgrade/

## 四、Ubuntu 桌面版升级教程
在 2022 年 4 月 21 日当天或之后，直到 2022 年 07 月第一次修正版本发布，Ubuntu 都不会给予你任何可视化的升级提示，因为第一个修改版本被认为是初始错误被消除后最稳定的版本。

这意味着，你要么等到 7 月，你要么强制升级。如何强制升级？打开“软件和更新 Software and Updates”，转到“更新 Updates”标签页。更改“通知我有新的 Ubuntu 版本 Settings notify me of new ubuntu version” 为 “任意新的 Ubuntu 版本 any new version”。

在完成后，你应该会在桌面上看到一个升级提示。遵循屏幕上的指示，接着继续升级过程。

这个提示也会计算升级系统所列软件包所需的时间，因此，仔细的阅读更新程序的输出内容。在你准备好了以后，开始升级过程。

最后，在升级过程完成后，重新启动系统，享受全新的 Ubuntu 22.04 LTS Jammy Jellyfish。

参考：https://os.51cto.com/article/706965.html