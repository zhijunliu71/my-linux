# 通过 Apt（不是 Snap）安装 Firefox

虚拟机安装`firefox`可以用`Snap`，没有发生问题。但是笔记本电脑（物理机）安装的时候，出现`Firefox`无法启动的情况。  
好消息是您可以使用几个命令在 `Ubuntu 22.04`（或 22.10）上安装 `Firefox deb`。 您先添加 `Mozilla Team PPA`，从 PPA 安装 `Firefox deb`，然后“固定”包以确保以后不会重新安装 `Firefox Snap`。  
但是，在您进一步操作之前，您应该备份/导出任何重要的浏览器设置、书签和来自 `Firefox` 的其他数据，以免出现任何问题。

- 通过在新的*终端*窗口中运行以下命令来删除 `Firefox Snap`:

  ```bash
  sudo snap remove firefox
  ```

- 通过在同一*终端*窗口中运行以下命令，将 (Ubuntu) Mozilla 团队 PPA 添加到您的软件源列表中:

  ```bash
  sudo add-apt-repository ppa:mozillateam/ppa
  ```

- 接下来，更改 `Firefo`x 包优先级以确保 `PPA/deb/apt` 版本的 `Firefox` 是首选。 这可以使用 [来自 FosTips 的代码](https://fostips.com/ubuntu-21-10-two-firefox-remove-snap) 的滑行来完成（复制并粘贴整个代码，而不是逐行粘贴）:

  ```bash
  echo '
  Package: *
  Pin: release o=LP-PPA-mozillateam
  Pin-Priority: 1001
  ' | sudo tee /etc/apt/preferences.d/mozilla-firefox
  ```

- 由于您（希望）希望自动安装未来的 `Firefox` 升级，[Balint Reczey 在他的博客上分享了一个简洁的命令](https://balintreczey.hu/blog/firefox-on-ubuntu-22-04-from- deb-not-from-snap/) 确保它发生:

  ```bash
  echo 'Unattended-Upgrade::Allowed-Origins:: "LP-PPA-mozillateam:${distro_codename}";' | sudo tee /etc/apt/apt.conf.d/51unattended-upgrades-firefox
  ```

- 最后，通过运行此命令通过 `apt` 安装 `Firefox`:

  ```bash
  sudo apt install firefox
  ```

一旦 `Firefox` 安装完成弹出打开应用程序启动器并单击 Firefox 图标以启动 `de-Snapped` 版本，您可以固定到 `Ubuntu dock`。