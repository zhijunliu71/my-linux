# 在 Debian 上安装 Docker 引擎

**其它版本，请参照官方网站：**

- [安装 Docker 引擎](https://docs.docker.com/engine/install/)
  - [在 Windows 上安装 Docker Desktop](https://docs.docker.com/desktop/windows/install/)
- 在 Linux 上安装 Docker 引擎
  - [在 Debian 上安装 Docker 引擎](https://docs.docker.com/engine/install/debian/#install-using-the-repository)
  - [在 Ubuntu 上安装 Docker 引擎](https://docs.docker.com/engine/install/ubuntu/)
  - [在 CentOS 上安装 Docker 引擎](https://docs.docker.com/engine/install/centos/)
  - [在 RHEL 上安装 Docker 引擎](https://docs.docker.com/engine/install/rhel/)



## 1. 卸载旧版本

- 旧版本的 Docker 称为 docker、docker.io 或 docker-engine。如果安装了这些，请卸载它们：

  ```shell
  sudo apt-get remove docker docker-engine docker.io containerd runc
  ```



## 2. 使用存储库安装



### 2.1 设置存储库

1. 更新 apt 包索引并安装包以允许 apt 通过 HTTPS 使用存储库：

   ```shell
   sudo apt-get update
   
   sudo apt-get install \
      ca-certificates \
      curl \
      gnupg \
      lsb-release

2. 添加 Docker 的官方 GPG 密钥：

   ```shell
   curl -fsSL https://download.docker.com/linux/debian/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
   ```

3. 使用以下命令设置 stable 存储库。要添加 nightly 或 test 存储库，请在以下命令中的 stable 后面添加单词 nightly 或 test（或两者）。[了解 nightly 和 test 频道](https://docs.docker.com/engine/install/)。

   ```shell
   echo \
     "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/debian \
     $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
   ```

### 2.2 安装 Docker 引擎

1. 更新apt包索引，安装最新版本的Docker Engine和containerd，或者下一步安装具体版本：

   ```shell
   sudo apt-get update
   
   sudo apt-get install docker-ce docker-ce-cli containerd.io
   ```
   
   上述指令安装了 Docker 的最新版本，如果需要安装指定版本，请参阅后续的指令，否则无需关系后续指令。

2. 要安装特定版本的 Docker Engine，请在 repo 中列出可用版本，然后选择并安装：

   列出您的存储库中可用的版本：

   ```shell
   apt-cache madison docker-ce
   ```

   使用第二列中的版本字符串安装特定版本，例如 5:18.09.1~3-0~debian-stretch。

   ```shell
   sudo apt-get install docker-ce=<VERSION_STRING> docker-ce-cli=<VERSION_STRING> containerd.io
   ```
   
3. 通过运行 hello-world 映像来验证 Docker 引擎是否已正确安装。

   ```shell
   sudo docker run hello-world
   ```
   
   此命令下载一个测试镜像并在容器中运行，当容器运行时，它会打印一条消息并退出。

Docker Engine 已安装并正在运行。创建了 docker 组但未添加任何用户。您需要使用 sudo 运行 Docker 命令。继续 Linux postinstall 以允许非特权用户运行 Docker 命令和其他可选配置步骤..



## 3. 从软件包安装

如果你不能使用 Docker 的仓库安装 Docker Engine，你可以下载你的发行版的 .deb 文件并手动安装，每次升级 Docker 都需要下载一个新文件。

1. 转到 https://download.docker.com/linux/debian/dists/，选择您的 Debian 版本，然后浏览到 pool/stable/，选择 amd64、armhf 或 arm64，然后下载 Docker 引擎的 .deb 文件要安装的版本。

2. 安装 Docker Engine，将下面的路径更改为您下载 Docker 包的路径。

   ```shell
   sudo dpkg -i /path/to/package.deb
   ```
   
   Docker 守护进程自动启动。
   
3. 通过运行 hello-world 映像来验证 Docker 引擎是否已正确安装。

   ```shell
   sudo docker run hello-world
   ```
   
   此命令下载一个测试镜像并在容器中运行，当容器运行时，它会打印一条消息并退出。

Docker Engine 已安装并正在运行。创建了 docker 组，但未添加任何用户。您需要使用 sudo 运行 Docker 命令。继续 Linux 的安装后步骤以允许非特权用户运行 Docker 命令和其他可选配置步骤。



## 4. 赋予 Sudo 权限

1. 为避免每次运行 docker 命令时都输入 sudo，请将您的用户名添加到 docker 组。

   ```shell
   sudo usermod -aG docker ${USER}
   ```

2. 要应用新的组成员资格，请退出服务器并重新登录，或键入：

   ```shell
   su - ${USER}
   ```

   系统将提示您输入用户密码以继续。


## 5. 升级 Docker 引擎

- 要升级 Docker Engine，首先运行如下指令，然后按照安装说明，选择要安装的新版本。

  ```shell
  sudo apt-get update
  ```

  



## 6. 卸载 Docker 引擎

1. 卸载 Docker 引擎、CLI 和 Containerd 软件包：

   ```shell
   sudo apt-get purge docker-ce docker-ce-cli containerd.io
   ```

2. 主机上的镜像、容器、卷或自定义配置文件不会自动删除。删除所有镜像、容器和卷：

   ```shell
   sudo rm -rf /var/lib/docker
   
   sudo rm -rf /var/lib/containerd
   ```

您必须手动删除任何已编辑的配置文件。

