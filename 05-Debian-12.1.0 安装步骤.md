# Debian-12.1.0 安装步骤（虚拟机）

## 1. 安装过程

### 1.1 VirtualBox 安装

整体非常顺利，没有出现特殊情况。

### 1.2 把终端固定到快捷栏

方便以后的各种操作，后续各种操作基本都需要用到`终端`模块。需要修改显示尺寸，修改为 `160 x 32` 的大小。

## 2. 把用户名追加到sudoers群组

解决方案：
- （1）进入root模式，su，再输入正确密码，
- （2）通过cd /etc 进入etc文件中，
- （3）通过vim打开sudoers并编辑，把用户名追加到root后面。
  
  ```sh
  sudo nano /etc/sudoers
  sudo gedit /etc/sudoers
  ```

## 3. 驱动安装及配置系统

只有安装了驱动之后，才能够在虚拟机内全画面显示。

### 3.1 VirtualBox 安装

```
sudo bash ./VBoxLinuxAdditions.run
```

### 3.2 vboxadd.service 启动失败

- 执行如下指令：

  > 官方说明网址：https://forums.virtualbox.org/viewtopic.php?t=92116
  
  ```bash
  sudo apt install linux-headers-$(uname -r) build-essential dkms
  ```

  重新安装驱动也无法改变，参照了如下的URL：

  [VirtualBox上の ServerLinuxへの GuestAdditionsの導入方法](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=&cad=rja&uact=8&ved=2ahUKEwiv-u6kyKyCAxXwslYBHXdhDJsQFnoECAgQAQ&url=https%3A%2F%2Fqiita.com%2Fmiyagaw61%2Fitems%2Fbd577a2e08ec04081235&usg=AOvVaw17QqNrfd5nElHwiTd5Csbd&opi=89978449)

  [How to fix virtualbox startup error: "vboxadd-service ... failed!"](https://superuser.com/questions/298367/how-to-fix-virtualbox-startup-error-vboxadd-service-failed)
  
  [VBoxadd-service.service fails on startup - virtualbox.org](https://forums.virtualbox.org/viewtopic.php?t=92116)

可以非常有效的提高启动速度，否则会耽搁几秒钟。

### 3.3 扩展设置

启动 `扩展` 功能，设置桌面操作习惯。

### 3.4 添加日语输入法

- 如下指令添加 `Anthy` 输入法：

  ```bash
  sudo apt install ibus-anthy
  ```

  添加之后，可以通过图形界面，添加该输入法。

### 3.5 快速启动

画面启动的时候，会暂停`5`秒，可以进行菜单选择，在系统启动出现问题的时，可以通过其它启动项目，进行对应。

- 禁止该项，加快启动速度，大概是在`86`行和`90`行，2个地方需要从`5`修改为`0` 。
  
  ```bash
  sudo nano /boot/grub/grub.cfg
  sudo gedit /boot/grub/grub.cfg
  ```

### 3.6 设置自动登录

### 3.7 调整电源管理

## 4. 卸载原生liboffice

- 指令
  
  ```bash
  sudo apt-get remove libreoffice-common
  
  ## 再インストールの場合は
  sudo apt-get install libreoffice
  ```

