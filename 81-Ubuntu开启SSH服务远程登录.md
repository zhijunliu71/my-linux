# Ubuntu开启SSH服务远程登录

- 客户端：`openssh-client`
- 服务器端：`openssh-server`

安装：

- 如果你只是想登陆别的机器的SSH只需要安装openssh-client（ubuntu有默认安装）：

  ```shell
  sudo apt-get install openssh-client
  ```

- 如果要使本机开放SSH服务就需要安装openssh-server：

  ```shell
  sudo apt-get install openssh-server
  ```

- 查看是否已经安装了SSH的相关服务：

  ```shell
  dpkg -l | grep ssh
  ```

- 确认服务是否已经启动：

  ```shell
  ps -e | grep ssh
  ```

  如果看到sshd那说明ssh-server已经启动了。
  
  如果没有则可以这样启动：sudo /etc/init.d/ssh start或sudo service ssh start

  ```shell
  sudo /etc/init.d/ssh start
  sudo service ssh start
  ```

  配置相关：
  
  ssh-server配置文件位于/etc/ssh/sshd_config，在这里可以定义SSH的服务端口，默认端口是22，你可以自己定义成其他端口号，如222。（或把配置文件中的”PermitRootLogin without-password”加一个”#”号,把它注释掉，再增加一句”PermitRootLogin yes”）
  然后重启SSH服务：

  ```shell
  sudo /etc/init.d/ssh stop
  sudo /etc/init.d/ssh start
  ```

- 登陆SSH服务器：

  ```shell
  ssh username@192.168.1.103
  
  ## 其中，username为192.168.1.103机器上的用户，需要输入密码。
  断开连接：
  exit
  ```
