# 将 Thunderbird 的数据迁移到另一台计算机

Thunderbird把你的数据保存在单独的位置，并不在应用目录下，它就是你的配置文件夹。要移动该数据，请复制配置文件夹到目标计算机的对等目录。
注意：如果你使用配置管理器定制了 Thunderbird 配置文件夹存储的远程路径，请在目标电脑上 创建一个新配置 并使之使用同样的路径。Thunderbird 安装时会使用现有的数据。

## 在来源电脑上

- 连接一个用来转移数据的设备，可以是 USB 盘，也可以是远程存储设备。
- 点击菜单按钮 Fx57Menu，然后找到 帮助 并选择 故障排除信息。
- 找到 "配置文件夹" 选项。点击 打开目录。你的 Thunderbird 配置文件夹会在 文件浏览器 里打开。
- 关闭Thunderbird。
- 从此 Thunderbird 配置文件夹，向上2级目录。
- 右击 **.thunderbird** 文件夹，并选择复制。
- **.thunderbird** 是一个隐藏文件夹。请确保Linux设置为显示隐藏文件夹。
- 右击 转移数据的设备，然后选择粘贴。
- 如果你使用的本地存储设备，请安全移除。 

注意：以上步骤也可以用来制作 Thunderbird 的配置备份。

## 在目标电脑上
- 连接刚才取下来的、已经存储了 Thunderbird 配置文件的设备。
- 右击 你在上一步复制好的 Thunderbird 文件夹，然后选择粘贴。
- 打开 Thunderbird，当账号设置窗口弹出时，关闭它。
- 点击菜单按钮Fx57Menu，然后找到帮助并选择故障排除信息。
- 找到 "配置文件夹" 选项。点击打开目录。
- 你的Thunderbird配置文件夹会在文件浏览器里打开。
- 关闭Thunderbird。
- 从此Thunderbird配置文件夹，向上2级目录。
- 右击 在此文件夹内，选择粘贴。
- 提示时，选择替换现有数据。 

## 参照资源

- [Thunderbird官方相关资料](https://support.mozilla.org/zh-CN/kb/%E5%B0%86%20Thunderbird%20%E7%9A%84%E6%95%B0%E6%8D%AE%E8%BF%81%E7%A7%BB%E5%88%B0%E5%8F%A6%E4%B8%80%E5%8F%B0%E8%AE%A1%E7%AE%97%E6%9C%BA)
- http://mzl.la/1ApHiU3