# Linux相关资讯



- ### 2022.04.23

  尝试了 Ubuntu 21.10 版本，感觉鼠标的反应速度要比 20.04 好很多，有点儿类似 Debian 的感觉；但是很多方便还是不如 Debian，可惜的是 Debian 在 vmware 上运行不好（依然是经常卡死，应该是 vmware 驱动对 Debian 的图形界面支持不好），当前只能在 VirtualBox 上应用。

## Linux教程

| 章节                            | 链接                                                         | 备注 |
| ------------------------------- | ------------------------------------------------------------ | ---- |
| 第 1 章 GNU/Linux 教程          | [链接](https://www.debian.org/doc/manuals/debian-reference/ch01.zh-cn.html) |      |
| 第 2 章 Debian 软件包管理       | [链接](https://www.debian.org/doc/manuals/debian-reference/ch02.zh-cn.html) |      |
| 第 3 章 系统初始化              | [链接](https://www.debian.org/doc/manuals/debian-reference/ch03.zh-cn.html) |      |
| 第 4 章 认证和访问控制          | [链接](https://www.debian.org/doc/manuals/debian-reference/ch04.zh-cn.html) |      |
| 第 5 章 网络设置                | [链接](https://www.debian.org/doc/manuals/debian-reference/ch05.zh-cn.html) |      |
| 第 6 章 网络应用                | [链接](https://www.debian.org/doc/manuals/debian-reference/ch06.zh-cn.html) |      |
| 第 7 章 GUI（图形用户界面）系统 | [链接](https://www.debian.org/doc/manuals/debian-reference/ch07.zh-cn.html) |      |
| 第 8 章 国际化和本地化          | [链接](https://www.debian.org/doc/manuals/debian-reference/ch08.zh-cn.html) |      |
| 第 9 章 系统技巧                | [链接](https://www.debian.org/doc/manuals/debian-reference/ch09.zh-cn.html) |      |
| 第 10 章 数据管理               | [链接](https://www.debian.org/doc/manuals/debian-reference/ch10.zh-cn.html) |      |
| 第 11 章 数据转换               | [链接](https://www.debian.org/doc/manuals/debian-reference/ch11.zh-cn.html) |      |
| 第 12 章 编程                   | [链接](https://www.debian.org/doc/manuals/debian-reference/ch12.zh-cn.html) |      |



## Linux下载网址

| Linux                | 下载网址                      | 备注                                                         |
| -------------------- | ----------------------------- | ------------------------------------------------------------ |
| **CentOS**           | https://www.centos.org/       | 由开源社区发布并运营的知名Linux操作系统，提供了一个强大的开源生态环境。以“免费的RHEL系统”广泛被人们所熟知，从2021年起主要运营CentOS Stream项目。 |
| **红帽RHEL**         | https://downloads.redhat.com/ | RHEL是全世界使用最广泛的Linux系统之一，在世界500强企业中所有的航空公司、电信服务供应商、商业银行、医疗保健公司均无一例外有基于红帽产品所提供的服务。 |
| **Fedora**           |                               | Fedora是桌面版本系统，可以理解成是微软公司的Windows XP或者Windows 10，定位是应付日常的工作需要，而不会追求稳定性的人群（服务器经常几年不关机）。 |
| Debian               |                               | Debian系统具有很强的稳定性和安全性，并且提供了免费的基础支持，可以良好的适应各种硬件架构，以及提供近十万种不同的开源软件，在国外有很高的认可度。 |
| Ubuntu               |                               | Ubuntu是一款桌面版本系统，基于Debian系统为蓝本进行修改和衍生而来，发布周期为六个月。这个词代表了一种谦卑、感恩的价值观，寓意非常好，中文音译为乌班图。 |
| openSUSE             |                               | 一款源自德国的Linux操作系统，在全球范围内有着不错的声誉及市场占有率。桌面版本简洁轻快易于使用，而服务器版本则功能丰富极具稳定性，即便菜鸟也能轻松上手。 |
| Kali                 |                               | 前身叫做BackTrack，设计的用途就是用于数字鉴识和渗透测试，内置有600多款网站及系统的渗透测试软件——包括大名鼎鼎的Nmap、Wireshark、sqlmap等等。 |
| Gentoo               |                               | Gentoo系统中任何一部分功能都允许用户重新编译，包括最基本的系统库和编译器都可以。用户可以选择自己喜欢的补丁或者插件进行定制，因此具有极高的自定制性。 |
| 深度操作系统(deepin) |                               | 系统是由武汉深之度科技有限公司于2011年基于Debian系统衍生而来的，目前累计下载已近1亿次，提供32种语言版本，用户遍布一百余个国家。 |
| ArchLinux            |                               | 系统只包含核心组件，采用滚动发行模式来获取最新的完整系统和软件，“保持简单和愚蠢”，注重代码正确、优雅和极简主义，帮助用户去深入理解系统的操作。 |
| Linux Mint           |                               | 一款于2006年发行的Linux操作系统，基于Ubuntu和Debian系统二次研发衍生而来，采用了最新版本的内核，拥有成熟的软件管理模式。 |
| FreeBSD              |                               | FreeBSD 先进的网络、安全性和存储方面的特色使得它成为许多大型网站以及最普遍的嵌入式网络与存储设备的平台选择。 |
| Oracle Linux         |                               | Oracle Linux 是一个开放、全面的操作环境，提供虚拟化、管理、云原生计算工具和操作系统，通过一个统一的支持解决方案满足您的业务需求。 |
| OpenBSD              |                               | 起源于加拿大，强调便携性、标准化、准确性、生产安全性的安全Linux系统。最初是基于NetBSD源码，如今已经是重要的衍生操作系统之一。 |
| CoreOS               |                               | 一款轻量级Linux操作系统，为了实现计算机集群的基础建设而构造，专注于自动化，轻松部署，安全，可靠，规模化。提供了容器内部署应用所需的一系列服务和工具。 |
| SmartOS              |                               | 专用于本地容器的虚拟化管理程序和轻量级容器的Linux操作系统，常被用于公有云或私有云中，能够安全、高效、方便的托管容器，更好的融合容器和虚拟机管理程序。 |
| Linux Lite           |                               | 对新手最友好的Linux操作系统，免费且易于上手。号称通往Linux世界的大门。 |
| Puppy Linux          |                               | Puppy Linux是个相当小巧的Linux发行版，虽然它只有几十MB的大小，但它自带了大量的扫描仪、打印机、数码相机 的驱动以及音乐录制/编辑/转换软件。 |
| ClearOS              |                               | 一款基于RHEL衍生而来的系统，通常被用于中小企业的网站服务器，可以作为分布式环境的基础设施，也会被搭建成网关服务器。 |
| 4MLinux              |                               | 极其“迷你”的Linux操作系统，仅包含了一些基础软件，除了小巧，还真没什么特色。 |
| mageia               |                               | 发布于2010年的年轻操作系统，由非盈利社区运营，完全开源和自由。不论是被用作桌面端还是服务器都是很安全的选择。 |
| SteamOS              |                               | Steam游戏平台旗下产品，基于Debian系统研发而来，目的是结合大尺寸屏幕提升游戏体验，提高图片和音效的质感，对游戏程序的兼容性更高~ |
| Lubuntu              |                               | 一个Ubuntu系统的衍生分支，追求干净、快捷、轻量级，对于硬件需求非常低。相比来说，Lubuntu不仅对硬件的要求更低了，而且软件包之间的依赖关系也降低了。 |
| Solus                |                               | 专为客户端设计的Linux操作系统，每周都会在系统上滚动更新一些新工具，交流社群很活跃，千万别用在服务器上。 |
| Manjaro              |                               | 基于Arch Linux衍生而来，拥有自己独立的软件仓库，想让用户更好的使用Linux系统。号称对Wine模拟软件兼容性更好，可以在上面运行绝大部分的Windows程序。 |
| KNOPPIX              |                               | 一款不需要安装的Linux系统，平时可以放到U盘或刻录到光盘中，随插随用，可以用于临时演示和系统急救等场景，对于硬件的支持性很好，一般的电脑都能运行。 |
| Slackware            |                               | 一款颇具个性的Linux系统，永远保持简洁性是Slackware的设计理念，不包含任何多余的服务、工具，甚至是命令。所以系统拥有了更高效的运行环境，但对新手很不友好。 |
| Oracle Solaris       |                               | 最早由Sun公司基于BSDUnix研发的系统叫做Solaris，后被Oracle公司并购后改名Oracle Solaris。简单稳定，值得信赖。 |
| Asianux              |                               | 由中国、日本、韩国的三家开发公司联合研发的Linux操作系统，想做一款亚洲人通用的系统，致力于可靠性、弹性高、易于管理的兼容性平台。 |
| Miracle Linux        |                               | 号称对Oracle数据库支持最良好的操作系统，但被RHEL系统挤压到没有市场。现在该项目接近关闭，开发人员转去负责了Asianux。 |
| 红旗(redflag)        |                               | 中科院背景，北京中科红旗软件开发公司推出的一款Linux操作系统，包含桌面版、工作站版、数据中心版等类型产品，是国内较为成熟的自主系统。 |
| Elementary OS        |                               | 快速、开源、注重隐私的 Windows / macOS 替代方案，远离浮世喧嚣，让你专注于当前需要完成的事情上。 |
| Univention           |                               | 实现低成本运营和轻松管理服务器的新选择，无论您使用 Microsoft Windows、Mac OS X 还是 Linux 系统，UCS 都非常适合管理分布式异构和虚拟化 IT 环境。 |
| Pop!_OS              |                               | 适合于 STEM 和创意专业人士的操作系统，可以在这里随意的发现和创作，基于你非凡的好奇心，释放自己的无限潜力。 |
| Zorin OS             |                               | 号称要替代Windows和macOS系统，成为一个让计算机更快、更强大、更安全且尊重用户隐私的新兴系统。 |
| Nitrux               |                               | 基于Debian系统衍生而来，曾经被评为外观最好看的Linux系统之一。操作系统界面美观、简洁、流畅，提升用户的操作体验。 |
| eXtern OS            |                               | 独特的用户界面和用户体验，使用JavaScript技术提供的支持，利用节点模块的强大功能，让App开发工作更具创造性。 |
| Ubuntu Kylin         |                               | 优麒麟是CCN开源创新联合实验室和麒麟软件有限公司开发的开源Linux操作系统，基于Ubuntu系统衍生而来，主打“友好易用，简单轻松”的桌面环境。 |
| BackBox              |                               | 提供了当前最流行的和最知名的一些黑客工具，设计理念是让用户更容易上手使用，提供了体积最小但功能基本完整的桌面环境。 |
| Parrot Security os   |                               | 基于Debian系统衍生而来，是Parrot Security公司的旗舰产品，是一款便携式的实验室系统，从渗透测试到数字取证和逆向功能，满足一切网络安全的需求。 |
| Cyborg Hawk          |                               | 一款由某印度黑客基于ubuntu开发的Linux操作系统，集成了一些最新的网络安全工具和独家秘方，用于黑客的渗透测试和网络安全等需求。 |
| BlackArch            |                               | 基于Arch Linux系统衍生开发而来，是一款专用于渗透测试和安全研究人员的操作系统，包含约3000余个当前最新的渗透工具包，并且还在持续更新。 |
| ArchStrike           |                               | 基于Arch Linux系统衍生而来，适用于渗透测试和安全专业人员，配备了大量渗透测试和网络安全相关的工具，号称有数千款之多。 |
| Rocky Linux          |                               | 起点很高的一款Linux操作系统，由原CentOS项目的创始人Gregory Kurtzer领导研发，挑战红帽公司带来的“垄断”，或许成为新一代浴火重生的“CentOS”系统？ |
| StartOS              |                               | 广东爱瓦力科技股份有限公司发行的开源操作系统，据称更符合国人的使用习惯，预装常用的软件，满足日常办公的需求。 |
| 凝思                 |                               | 北京凝思软件股份有限公司研发的操作系统，据称拥有完全自主知识产权，在科技部863信息安全项目和发改委的支持下快速发展，被政务机关广泛应用。 |
| 中科方德             |                               | 中科方德软件有限公司成立于2006年，是“基础软件国家工程研究中心”项目法人单位，该中心是目前基础软件领域唯一的国家级工程技术研究中心。 |
| 新支点               |                               | 广东中兴新支点技术有限公司成立于2004年，是中兴通讯的全资子公司，旗下有新支点工业操作系统、服务器操作系统、桌面操作系统、高可用集群软件等产品。 |
| 普华Linux            |                               | 普华基础软件股份有限公司是中国电子科技集团公司整合集团优势资源共同投资设立的，注册资金2.89亿元人民币，是中国电科发展基础软件的重要平台。 |
| RT-Thread            |                               | RT-Thread 实时操作系统遵循 Apache 许可证 2.0 版本，实时操作系统内核及所有开源组件可以免费在商业产品中使用，不需要公布应用程序源码，没有潜在商业风险。 |
| Aosc Linux           |                               | AOSC OS 是一个通用的 Linux 发行版，致力于简化用户体验并改进免费和开源软件以提高日常工作效率。 |
| kubuntu              |                               | Kubuntu 将 Ubuntu 与 KDE 和美妙的 Plasma 桌面结合在一起，为您带来一整套应用程序。该安装包括可在启动时使用的生产力、办公、电子邮件、图形、摄影和音乐应用程序。 |
| netrunner            |                               | 基于Ubuntu的衍生系统，着眼于桌面计算。该发行引以为豪的是一份精心修改的KDE &nbsp;4桌面，它带有很多集成进来的GNOME应用程序，从而以混合的形式向用户提供流行的和强大的应用软件。 |
| Xubuntu              |                               | Xubuntu 附带 Xfce，它是一个稳定、轻量级和可配置的桌面环境。非常适合那些想要充分利用台式机、笔记本电脑和上网本的人。 |
| Runtu                |                               | Runtu是来自俄罗斯的桌面Linux发行，它基于Ubuntu。其特色在于对俄语的完整支持，以及各种各样的额外应用软件、工具、多媒体编码解码器。 |
| GalliumOS            |                               | 一个快速、轻量级的 Linux 发行版，适用于 Chromebook 和 Chromebox。 GalliumOS 提供了一个完整的 Linux 环境，具有完整的硬件支持以及精心调整的内核。 |
| ChaletOS             |                               | ChaletOS 这个名字来源于瑞士山间房屋的风格。 这些房子的概念类似于我们在制作这个系统时的概念：简单、美观和可识别性，想让新用户有宾至如归的感觉。 |
| Ututo                |                               | 专注于完全免费的社交应用软件为特点的操作系统，2003年被自由软件基金会认定为100%免费的开源系统，2006年被阿根廷共和国众议院评选为“公益项目”。 |
| madbox               |                               | Madbox是一个基于 Ubuntu 的发行版，使用 Openbox 作为窗口管理器，暂无其他特色。 |
| BunsenLabs           |                               | 基于Debian系统衍生而来，提供轻量级且易于定制的 Openbox 桌面。 |
| Amazon Linux         |                               | 一个由Amazon公司支持和维护的Linux操作系统， 旨在为 Amazon EC2 上运行的应用程序提供稳定、安全和高性能的执行环境。 |
| Core Linux           |                               | 它不是一个完整的系统，也不能完全支持所有硬件，仅仅是具有有线互联网访问权限的最小系统哦那个所需的核心，像TinyCore才16MB这么大。 |
| Astra Linux          |                               | 俄罗斯IT行业领导者RusBITech-Astra LLC公司出品，最早于2008年公布下载地址，号称此系统是用于确保俄罗斯技术主权在全球IT行业占领导地位。 |
| Ubuntu MATE          |                               | 一个稳定的、易于使用的操作系统，具有高度可配置的桌面环境，适合哪些希望充分利用计算机的达人们，在一些旧硬件的电脑上安装上速度挺快的。 |
| PCLinuxOS            |                               | 一个免费且易于使用的Linux操作系统，已经支持了85种国家语言，号称更加可靠，让用户不必担心病毒、广告软件、特洛伊木马等程序感染心爱的计算机。 |
| KDE Neon             |                               | 基于Ubuntu系统衍生而来，打包了来自KDE社区最热门软件的Linux操作系统，让用户更好的享受KDE社区带来的更好、更多的新软件。 |
| Trisquel GNU/Linux   |                               | 面向于家庭和办公室应用场景的Linux系统，新程序更容易被找到和安装，以及有更好的辅助功能，让系统和软件功能更加无障碍。 |
| Bodhi Linux          |                               | 一款轻量级的操作系统，具有快速且完全可定制的Moksha桌面，以极简主义、资源效率和用户选择而闻名，而不是预装一堆不必要的应用程序。 |
| Mandrake             |                               | OpenMandriva协会的目标是开发一个实用的Linux操作系统，为从新手到开发人员的每个人都能获得最佳用户体验而努力，努力在新功能和稳定性之间求平衡。 |
| Qubes OS             |                               | 一款面向于单用户的免费、开源、安全的操作系统，Qubes OS 利用基于 Xen 的虚拟化来创建和管理称为 qubes 的隔离隔间。 |
| FatDog64 Linux       |                               | 最早是作为Puppy Linux的一款内置应用程序而衍生创建的，目前已经是一款成熟的独立操作系统，小巧、快速和高效。 |
| DietPi               |                               | 一款基于Debian衍生而来的操作系统，针对CPU和内存资源使用量进行了高度的优化，号称要让计算机发挥出最大的潜力。 |
| Robolinux            |                               | 创始人是一名旅行迷，走遍了世界各国，听取用户的直接反馈。他号称是要编写出一款功能强大、安全可靠、显著提升个人计算机工作效率的Linux操作系统。 |
| Clear Linux          |                               | 提供所需的大量软件及功能，默认安装了GNOME Shell、GNOME Terminal、Gedit、gFTP、Mozilla Firefox、Mozilla Thunderbird等等常用软件。 |
| CAINE                |                               | 全名是计算机辅助调查环境，是意大利的一款Linux操作系统，提供了完整的软件模块和有好的图形界面，主要用于数字调查员使用的取证环境。 |
| Septor Linux         |                               | 一款能够让用户完美匿名的Linux操作系统，基于Debian系统衍生而来，集成并定制了 KDE Plasma 桌面和 Tor 技术。 |

> https://www.linuxdown.com/



## Linux综合学习

- 日语：各种系统安装  
  https://www.server-world.info/
- Docker中文文档  
  http://www.dockerinfo.net/document
- Kubernetes中文文档   
  https://www.kubernetes.org.cn/docs  
  http://docs.kubernetes.org.cn/



## Docker安装-官方教程

- https://docs.docker.com/engine/install/ubuntu/

### 第 1 章 GNU/Linux 教程

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/zhijunliu71/my-linux.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/zhijunliu71/my-linux/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!).  Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
