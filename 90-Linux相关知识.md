# Linux相关知识



## 1. 系统信息查询

### 1.1 cpu个数及核心数

总核数 = 物理CPU个数 X 每颗物理CPU的核数
总逻辑CPU数 = 物理CPU个数 X 每颗物理CPU的核数 X 超线程数

- 查看物理CPU个数

  ```bash
  cat /proc/cpuinfo| grep "physical id"| sort| uniq| wc -l
  ```

- 每个物理CPU中core的个数(即核数)

  ```bash
  cat /proc/cpuinfo| grep "cpu cores"| uniq
  ```

- 查看逻辑CPU的个数

  ```bash
  cat /proc/cpuinfo| grep "processor"| wc -l
  ```

- 查看线程数

  ```bash
  grep 'processor' /proc/cpuinfo | sort -u | wc -l
  ```

  注意：此处查看的线程数是总的线程数，可以理解为逻辑cpu的数量。

### 1.2 查看磁盘空间

- 查看磁盘剩余空间

  ```bash
  df -hl
  ```

- 查看每个根路径的分区大小

  ```bash
  df -h [目录名]
  ```

- 返回该目录的大小

  ```bash
  du -sh [文件夹]
  ```

- 返回该文件夹总M数

  ```bash
  du -sm
  ```

### 1.3 查看内存大小

- 命令查询

  ```bash
  free -m
  ```

  > `# free 默认单位为 KB`   
  > `# [Mem:][total] 的值就是当前系统的内存大小 (MB)`

- 通过指定文件查询

  ```bash
  grep MemTotal /proc/meminfo
  ```

- top 命令查看

  top 命令可以查看系统的 CPU、内存、运行时间、交换分区、执行的线程等信息

  ```bash
  top
  
  # 也可以使用增强版 top
  apt-get install htop
  htop
  ```

  

- free 命令详解：

  > 功能说明：显示内存状态。
  > 　　语　　法： free [-bkmotV][-s ]
  > 　　补充说明：free指令会显示内存的使用情况，包括实体内存，虚拟的交换文档内存，共享内存区段，连同系统核心使用的缓冲区等。
  > 　　参　　数：
  > 　　-b 　 以Byte为单位显示内存使用情况。
  > 　　-k 　 以KB为单位显示内存使用情况。
  > 　　-m 　 以MB为单位显示内存使用情况。
  > 　　-o 　 不显示缓冲区调节列。
  > 　　-s 持续观察内存使用状况。
  > 　　-t                        　 显示内存总和列。
  > 　　-V 　 显示版本信息。
  >
  > 作者：音心
  > 链接：https://juejin.cn/post/6844903552784007175
  > 来源：稀土掘金
  > 著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。

- free -m 查询后显示信息详解

  > 第二行的 -/+buffers/cache 是当前实际被使用的内存和空闲内存空间，具体解释如下：mem 行显示了从系统角度来看内存使用的情况total 是系统可用的内存大小 , 数量上等于系统物理内存减去内核保留的内存buffers 和 cached 是系统用做缓冲的内存buffers                        与某个块设备关联 , 包含了文件系统元数据 , 并且跟踪了块的变化cache 只包含了文件本身-/+ buffers/cache 行则从用户角度显示内存信息, 可用内存从数量上等于 mem 行 used 列值减去 buffers 和 cached                        内存的大小；因为 buffers 和 cached 是操作系统为加快系统运行而设置的 , 当用户需要时 , 可以直接为用户使用top 和 vmstat 也可以显示系统内存的信息 , 和 free 的显示结果类似
  >
  > 作者：音心
  > 链接：https://juejin.cn/post/6844903552784007175
  > 来源：稀土掘金
  > 著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。

## 2. 创建RAM虚拟硬盘

> https://askubuntu.com/questions/152868/how-do-i-make-a-ram-disk

1. `tmpfs` 文件系统是一个 `RAMDISK`。 下面将创建一个始终可用的 `2GB` 的 `RAMDISK`。

   ```bash
   sudo mkdir -p /media/ramdisk
   sudo mount -t tmpfs -o size=2048M tmpfs /media/ramdisk
   ```

2. `ramdisk` 文件夹归 `root` 所有，因为它在重新启动时可用。 `ramdisk` 权限应该可供所有人写入。 `tmpfs` 默认权限 (`chmod 1777`) 是正确的。

   ```bash
   sudo chmod 1777 /media/ramdisk
   ```

   > drwxrwxrwt 2 root root 180 Apr 23 07:34 /media/ramdisk

3. 要使 `ramdisk` 永久可用，请将其添加到 `/etc/fstab`。

   ```bash
   grep /media/ramdisk /etc/mtab | sudo tee -a /etc/fstab
   ```

   您将看到该行从 mtab 移至 fstab。 它看起来像这样。

   > tmpfs /media/ramdisk tmpfs rw,size=2048M 0 0

`RAMDISK` 在您使用之前不会消耗内存。 在最大系统负载期间仔细检查您的内存要求。 如果 `RAMDISK` 太大，您的系统将消耗交换存储来弥补差额。

要调整 `RAMDISK` 的大小，请编辑 `/etc/fstab` 并通过重新安装 `ramdisk` 进行验证（您将丢失当前的 `RAMDISK` 内容，就像重新启动时一样）。 

- 下面将`ramdisk`的大小改为`512M`

  ```bash
  # Check the existing ramdisk size.
  df /media/ramdisk
  # change size=512M for a 512 megabyte ram drive.
  sudo vi /etc/fstab
  # Remount the ramdisk, you will lose any existing content.
  sudo mount -a /media/ramdisk
  # Verify the new ramdisk size.
  df /media/ramdisk
  ```

  

`Linux` 提供了一个任何用户都可以使用的 `tmpfs` 设备，`/dev/shm`。 默认情况下，它不会安装到特定目录，但您仍然可以将其用作目录。  
只需在 `/dev/shm` 中创建一个目录，然后将其符号链接到您想要的任何位置。 您可以向创建的目录授予您选择的任何权限，以便其他用户无法访问它。  
这是一个支持 `RAM` 的设备，因此默认情况下内存中存在内容。 您可以在 `/dev/shm` 中创建您需要的任何目录。  
当然，放置在这里的文件在重新启动后将无法保存，并且如果您的计算机开始交换，`/dev/shm` 将无法帮助您。

## 3. 解决 snap-store 无法更新

- 自身不能更新自身，按照如下指令进行更新：

  ```bash
  ## 终端输入 
  sudo snap refresh snap-store
  ## 若显示“正在运行”，kill 进程号
  kill 进程号
  ## 再次执行 
  sudo snap refresh snap-store
  ```

  

