# 删除Linux内无需内核

## 目录

[[_TOC_]]

## 1.  更新到最新内核

- 查看可用的内核更新命令

  ```bash
  apt list --upgradable | grep linux-
  ```

- 桌面版：

  ```bash
  sudo apt update
  sudo apt upgrade
  # sudo apt install linux-generic
  ```

- 服务器版：

  ```bash
  sudo apt update
  sudo apt upgrade
  # sudo apt install linux-server
  ```

新版本的内核及 Firefox 等，基本采取 SNAP 方式，使用如下指令进行更新。

- 更新指令：

  ```bash
  sudo snap refresh
  ```

## 2.  系统内核版本调查

- 查询当前内核

  ```bash
  ## 查询当前应用中的内核
  $ uname -r
  3.13.0-74-generic
  ```

- 查询所有内核

  ```shell
  ## 查询所有的内核信息
  $ dpkg --get-selections | grep linux-
  linux-firmware                                  install
  linux-generic                                   install
  linux-headers-3.13.0-67                         install
  linux-headers-3.13.0-67-generic                 install
  linux-headers-3.13.0-71                         install
  linux-headers-3.13.0-71-generic                 install
  linux-headers-3.13.0-74                         install
  linux-headers-3.13.0-74-generic                 install
  linux-headers-generic                           install
  linux-headers-server                            install
  linux-image-3.13.0-65-generic                   deinstall
  linux-image-3.13.0-67-generic                   install
  linux-image-3.13.0-71-generic                   install
  linux-image-3.13.0-74-generic                   install
  linux-image-extra-3.13.0-65-generic             deinstall
  linux-image-extra-3.13.0-67-generic             install
  linux-image-extra-3.13.0-71-generic             install
  linux-image-extra-3.13.0-74-generic             install
  linux-image-generic                             install
  linux-image-server                              install
  linux-libc-dev:amd64                            install
  linux-server                                    install
  ```
  


## 3.  删除不用使用内核

- 请根据内核信息，输入如下指令：

  ```bash
  ## headers
  sudo apt-get autoremove --purge linux-headers-3.13.0-{65,67,71}
  
  ## image
  sudo apt-get autoremove --purge linux-image-3.13.0-{65,71}
  ```

  卸载完内核后需要执行下列命令更新 grub

  ```bash
  sudo update-grub
  ```

- 重新启动之后，如下命令确认：

  ```bash
  ## 查询所有的内核信息
  dpkg --get-selections | grep linux-
  
  ## 如下指令删除系统垃圾。
  sudo apt autoremove
  sudo apt clean
  sudo apt autoclean
  ```
  
  可能无法解决冲突，但我建议尝试所有 apt 的解决问题的命令。 如果您还有其他包的问题，则很有用。
  
  ```bash
  sudo apt-get update
  sudo apt-get clean
  sudo apt-get autoremove
  sudo apt-get autoclean
  sudo apt-get --fix-broken install
  sudo apt-get upgrade
  sudo apt-get dist-upgrade
  ```
  

## 4. 禁用内核更新

- 使用命令禁用内核更新

  ```bash
  # 禁用内核更新
  sudo apt-mark hold linux-generic linux-image-generic linux-headers-generic
  # 恢复内核更新
  sudo apt-mark unhold linux-generic linux-image-generic linux-headers-generic
  ```

- 在 unattended-upgrades 配置文件中禁用内核更新

  ```bash
  sudo vi /etc/apt/apt.conf.d/50unattended-upgrades
  ```

- 查看安装的内核

  ```bash
  dpkg --list | grep linux-
  ```

- 清理多余的内核

  ```bash
  sudo apt purge linux-image-x.x.x-x  # x.x.x-x 代表内核版本数字
  sudo apt purge linux-headers-x.x.x-x
  sudo apt autoremove  # 自动删除不在使用的软件包
  
  #卸载完内核后需要执行下列命令更新 grub
  sudo update-grub
  ```

- 查看可用的内核

  ```bash
  apt list --upgradable | grep linux-
  ```

## 5. 禁止自动更新

- 修改配置文件

  ```bash
  # 关闭 Update-Package-Lists
  sudo sed -i.bak 's/1/0/' /etc/apt/apt.conf.d/10periodic
  # 关闭 unattended-upgrades
  sudo sed -i.bak 's/1/0/' /etc/apt/apt.conf.d/20auto-upgrades
  ## 也可以通过以下命令选择 No
  sudo dpkg-reconfigure unattended-upgrades
  ## 禁用 unattended-upgrades 服务
  sudo systemctl stop unattended-upgrades
  sudo systemctl disable unattended-upgrades
  ## 可选：移除 unattended-upgrades (sysin)
  #sudo apt remove unattended-upgrades
  ```

- 清空 apt 缓存

  ```bash
  # 可选：清空缓存
  sudo apt autoremove #移除不在使用的软件包
  sudo apt clean && sudo apt autoclean #清理下载文件的存档
  sudo rm -rf /var/cache/apt
  sudo rm -rf /var/lib/apt/lists
  sudo rm -rf /var/lib/apt/periodic
  ```

- 重置更新通知（更新提示数字）

  如果执行 `sudo apt update` 命令后，登录会提示如下：

  ```bash
  257 updates can be installed immediately.
  133 of these updates are security updates.
  To see these additional updates run: apt list --upgradable
  ```

  恢复原始状态：

  ```bash
  sudo vi /var/lib/update-notifier/updates-available
  # 第一行是空白
  
  0 updates can be installed immediately.
  0 of these updates are security updates.
  ```

  或者直接删除文件（推荐）：

  ```bash
  sudo rm -f /var/lib/update-notifier/updates-available
  ```

  删除后提示如下（`sudo apt update` 后会自动恢复）：

  ```bash
  The list of available updates is more than a week old.
  To check for new updates run: sudo apt update
  ```

  

## 6. 关于虚拟机的说明

- 如果是虚拟机的话，在处理之前，首先做一个快照，处理结束，确认启动无误之后，把这个快照删除，就能保持在最好状态。

- 当然，既然删除了内核，可以通过硬盘压缩，减小占用空间。这对虚拟机是非常有益的办法。