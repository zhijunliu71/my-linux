# 如何在 Linux 上创建 ISO 文件（CD 映像）



## 软件工具安装

- 命令：

  ```bash
  sudo apt install mkisofs
  ```

  

## 从文件或文件夹创建 iso 文件

- 命令：

  ```bash
  mkisofs -r -J -V <标签> -o <iso文件名> <目录名称>
  ```

- 示例：

  ```bash
  mkisofs -r -J -V "MyCD" -o cd_image.iso /home/hoge/
  ```



## 从 CD/DVD 驱动器创建 ISO 文件

- 命令1：

  ```bash
  dd if=/dev/cdrom of=<iso文件名>
  ```

  示例：

  ```bash
  dd if=/dev/cdrom of=cd_image.iso
  ```

- 命令2：

  ```bash
  mkisofs -r -J -o -V <ラベル> -o <iso文件名> <安装Mount位置>
  ```

  示例：

  ```bash
  mkisofs -r -J -V "MyCD" -o cd_image.iso /media/cdrom
  ```

  

