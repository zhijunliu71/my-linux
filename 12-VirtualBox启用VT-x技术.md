# 在 VirtualBox 中创建启用 VT-x 的虚拟机

> [Enable Nested VT-x / AMD-V] 在虚拟机设置中显示为灰色，无法启用。
> 这里姐扫如何启用它？

- **为什么不能启用VT-x只会吃？**
  确切信息未知，但无法启用 `Intel CPU (VT-x)` 并且可以为 `AMD CPU` 启用。

- **如果启用该技术，有什么好处？**
  能够在虚拟机中启动虚拟机。

- **如何启用该技术支持？**
  指定照常创建的 VM 的名称并执行以下命令。

  ```shell
  VBoxManage modifyvm [vm-name] --nested-hw-virt on
  ```

  