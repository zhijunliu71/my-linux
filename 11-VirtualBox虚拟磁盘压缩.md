## VirtualBox虚拟磁盘压缩

---

### 目次

[[_TOC_]]


---

### 概述

创建虚拟磁盘时，我推荐使用 [“可变大小存储”](http://vboxmania.net/content/)创建新虚拟机。  
使用“可变大小存储”，创建时的镜像文件很小，但是镜像文件会根据虚拟机的磁盘使用情况变大，最后镜像文件达到指定的磁盘大小，会很大。  
但是，即使你在巨大的虚拟磁盘中整理和删除文件，镜像文件也不会变小。一旦图像文件的区域被扩大，未使用的区域保持保留，通常是不可逆的。  
但是，您可以使用 [VBoxManage 命令](http://vboxmania.net/content/vboxmanage) 命令来删除未使用的空间并减小虚拟磁盘的大小。

###  1. 磁盘碎片整理

用 **0** 填充虚拟磁盘的可用空间，当操作系统删除一个文件时，它实际上只是删除了文件的索引信息，而不是实际的文件。  
**压缩前虚拟磁盘上的可用空间必须用零填充：**

- Windows 

  在 **Windows** 中，微软提供了 [sdelete tool](https://learn.microsoft.com/en-us/sysinternals/downloads/sdelete) 将可用空间免费覆盖为 **0**。所以我会用这个，[下载](https://download.sysinternals.com/files/SDelete.zip)、[地址2](https://technet.microsoft.com/en-us/sysinternals/bb897443)。

  启动**命令提示符**并使用 **-z** 选项运行 sdelete：

  ```sh
  sdelete -z C:
  sdelete64 -z C:
  ```

  > **对于文件与文件夹：**  
  >
  > ```bash
  > ## 直接删除 demo.txt 文件  
  > sdelete64.exe demo.txt
  > ## 直接删除 demo.txt 文件，并且进行 5 次覆盖
  > sdelete64.exe -p 5 demo.txt
  > ## 删除 c:\folders 以及子文件夹
  > sdelete64.exe -s c:\folders
  > ```
  >
  > **对于剩余空间：**  
  >
  > ```bash
  > ## 安全的清除 C 盘剩余空间，不影响已有文件
  > sdelete64.exe -c c:
  > ## 安全的清除 5 次 C 盘剩余空间
  > sdelete64.exe -c -p 5 c:
  > ## 对 C 盘剩余空间写零，适合虚拟磁盘优化
  > sdelete64.exe -z c:
  > ```
  >
  > 参照：https://www.ghacks.net/2017/11/14/delete-files-and-free-disk-space-securely-with-sdelete/

- Linux

  对于 ** Linux **，执行 **dd** 命令：

  ```sh
  dd if=/dev/zero of=zero bs=4k; \rm zero
  ```

  如果上述指令报告硬盘容量不足，可以用如下指令：

  ```bash
  sudo dd if=/dev/zero of=/EMPTY bs=1M
  sudo rm -f /EMPTY
  ```

  ※ `bs` 部分代表块大小，除非我们知道硬盘的最佳块大小，否则建议将其保留为 1M。

  ```
  dd: 写入 'zero' 出错: 设备上没有空间
  ```

  如果不通过 `count` 限制 `dd`，它将继续向磁盘写入数据，直到空间用完为止。  
  此错误很可能意味着 `dd` 命令已完成写入磁盘，例如当我们尝试通过向磁盘写入零或者随机数据来擦除磁盘时。  
  此时我们无需执行任何操作。   
   `dd` 忽略磁盘上的任何分区，并简单地写入直到到达指定的结尾，或者直到磁盘的结尾（如果没有提供结尾）。  
  我们可以通过count值限制要写入的块数。  

  ```bash
  dd if=/dev/zero of=/dev/sdb bs=1M count=100
  ```

### 2. 获取磁盘的 UUID

接下来，使用[VboxManage 命令](http://vboxmania.net/content/vboxmanage) 命令，获取要压缩的虚拟磁盘的 **UUID**。

- **Windows**：

  ```sh
  c:
  cd "C:\Program Files\Oracle\VirtualBox"
  VBoxManage.exe list hdds
  ```

  ```sh
  C:\Progra~1\Oracle\VirtualBox\VBoxManage.exe list hdds
  ```

- **Linux**：

  ```sh
  $ vboxmanage list hdds
  ```

### 3. 虚拟磁盘压缩

- VBoxManage 

  使用 ** VBoxManage ** 命令压缩虚拟磁盘：
  ```sh
  vboxmanage modifyhd [UUID] --compact
  VirtualBox Command Line Management Interface Version 3.2 (C) 2005-2010   Oracle Corporation, Inc. All rights reserved.
   0%...10%...20%...30%...40%...50%...60%...70%...80%...90%...100% >
  ```

  它释放了未使用的磁盘空间并减小了虚拟磁盘映像文件的大小。  
  或者直接压缩指定的 vdi 虚拟硬盘文件也可以。
  
  ```
  C:\Progra~1\Oracle\VirtualBox\VBoxManage modifyhd windows_xp_professional_sp3_x86.vdi –compact
  ```
  
- 如果你的虚拟硬盘是Vmware的VMDK格式，那就要麻烦点，因为VirtualBox不支持直接压缩VMDK格式，但是可以变通下：先转换成VDI并压缩，再转回VMDK。

  执行命令：

  ```
  VBoxManage clonehd "source.vmdk" "cloned.vdi" --format vdi
  VBoxManage modifyhd cloned.vdi --compact
  VBoxManage clonehd "cloned.vdi" "compressed.vmdk" --format vmdk
  ```

  事实上，执行命令的过程中可以发现：在从VMDK转换到VDI的过程中似乎已经做了压缩，文件大小已经减少了很多，第二条命令反而没见到文件大小有什么变化，所以这里第二条命令应该可以省略了。

## 附1：SDelete指令

windows系统磁盘空间清理工具。

### 1. 工具用途
在Windows下，我们可能希望安全的永久不可恢复地删除文件，或者希望将所有删除后可回收的空间置零，以便可以压缩硬盘或虚拟机里的系统可以压缩虚拟机硬盘的大小，这些都需要用到 Windows SDelete – 安全的、不可恢复的删除文件和擦除剩余空间 工具。

**SDelete 是一款安全的、不可恢复的删除文件和擦除剩余空间 的Windows 安全和实用工具。**

### 2. SDelete简介
SDelete 是著名的微软 Sysinternals 免费工具集软件之一，用来安全的、不可恢复的删除文件，以及多次擦除剩余空间。 使用SDelete可以安全的删除现有文件，以及安全地擦除磁盘的未分配部分中存在的数据（包括已经删除或加密的文件）。SDelete使用美国国防部清理标准DOD 5220.22-M，一旦使用SDelete删除，文件数据将永远消失。

SDelete 主要是命令行的方式，没啥美感的工具。不过，本身它主要也是将未用空间置零，也不需要太多界面，有界面反而会影响它的速度。
### 3. sdelete.exe特色介绍
sdelete.exe是微软推出的一款实用小工具，具有不经过回收站直接删除文件的功能，可以有效地保护已经删除的文件被还原。

sdelete.exe**用法：**

- sdelete [-p 传递] [-s] [-q] < 文件或目录 >

- sdelete [-p 传递] [-z|-c] [驱动器号]

**参数：**

- -p 传递-指定的覆盖传递 （默认值为 1）

- -q-不打印错误 （安静）

- -z-清除空间 [注意：它类似于-c 选项-z 使用随机字符串值的更安全地擦除的可用磁盘空间只]。

- -s-Recurse 子目录 [注意：所有子目录的内容包含在中删除或磁盘擦除]。

- -c-零 （适用于虚拟磁盘优化) 的可用空间 [注意：此选项擦除值为零的可用空间]。

### 4. 使用方法及举例
SDelete 用起来非常容易，可以使用命令行或GUI图形界面

1. 在命令行中使用sdelete

2. 使用图形界面的SDelete-Gui应用

#### 命令行的使用方法
这是一个命令行工具，你需要在cmd或powershell中调用它。

在命令行中使用 `sdelete` ，`cmd`或`powershell`中调用方法：

**指令：**

- sdelete [-p passes] [-r] [-s] [-q] <file or directory> [...]
- sdelete [-p passes] [-z|-c [percent free]] <drive letter [...]>
- sdelete [-p passes] [-z|-c] <physical disk number>

**参数：**

- -c 清理可用空间。 指定选项空间量  留给正在运行的系统免费使用。  
- -p 指定覆盖次数（默认为1）  
- -r 删除只读属性  
- -s 递归子目录  
- -z 零可用空间（适用于虚拟磁盘优化）  
- -nobanner 不显示启动标语和版权信息。磁盘必须没有任何卷才能被清理。
  

对于文件与文件夹[2]：

- sdelete64.exe demo.txt  
  直接删除 demo.txt 文件
- sdelete64.exe -p 5 demo.txt  
  直接删除 demo.txt 文件，并且进行 5 次覆盖
- sdelete64.exe -s c:\folders  
  删除 c:\folders 以及子文件夹

对于剩余空间[2]

- sdelete64.exe -c c:  
  安全的清除 C 盘剩余空间，不影响已有文件。
- sdelete64.exe -c -p 5 c:  
  安全的清除 5 次 C 盘剩余空间
- sdelete64.exe -z c:  
  对 C 盘剩余空间写零，适合虚拟磁盘优化

注意：请根据您实际的操作系统选择32/64位的sdelete  

- 如果是32位系统，使用sdelete  
- 如果是64位系统，可以使用sdelete，也可以使用sdelete64

### 5. 具体使用举例及经验提示
实测sdelete64.exe该工具对于缩小windows系统虚拟机的镜像、减小磁盘的占用空间有很大的效果，但使用时还需注意下列两点问题。（以c、e盘为例，其他的磁盘也一样）

1. 使用“sdelete64.exe -c c:”的命令时，该命令一般会重复执行两遍，且在执行清理的过程中被执行清理任务的虚拟机镜像的大小会扩大4-7倍，因此在执行此命令之前要确保磁盘有足够的空间，再执行“sdelete64.exe -z c:”置零C盘时，不会增长空间；

2. 使用“sdelete64.exe -z e:”置零E盘时，该命令一般会重复执行两遍，且在执行清理的过程中被执行清理任务的虚拟机镜像的大小会扩大2-3倍.

3. 重要提示：使用“sdelete64.exe -z 盘符路径”命令置零时不会损坏镜像，即使是中途因为磁盘空间不足而停止，当使用qemu-img convert -O的命令进行纯净镜像转化时镜像一定是缩小的，最终获得的镜像大小和-Z的完成率成反比，即-z的完成率越高，得到的最终镜像越小

4. 将已经完成清理的镜像转化为纯净的镜像
   - 直接在本磁盘转化新命名的镜像  
     qemu-img convert -O qcow2 CTC-WH-S1.qcow2 ATS-dadabase-server.qcow2  
   - 将转化的新命名镜像保存到别的路径  
     qemu-img convert -O qcow2 CTC-WH-S1.qcow2 /home/extend/ATS-dadabase-server.qcow2

### 6. SDelete-Gui的使用方法

1. 打开SDelete-Gui应用，点击ENABLE启用

2. 启用后，随便右键单击一个文件，你会看到右键列表中多了一个Secure Delete选项

这样你就可在档案管理器中，真正彻底地删除需要的文件。

打开SDelete-Gui应用，点击ENABLE启用
启用后，随便右键单击一个文件，你会看到右键列表中多了一个Secure Delete选项



## 附2：Linux 下的dd命令使用详解

> https://blog.csdn.net/jack909633117/article/details/116911783

### 1. dd命令的解释

dd：用指定大小的块拷贝一个文件，并在拷贝的同时进行指定的转换。  
注意：指定数字的地方若以下列字符结尾，则乘以相应的数字：b=512；c=1；k=1024；w=2

#### 参数注释：

1. if=文件名：输入文件名，缺省为标准输入。即指定源文件。< if=input file >

2. of=文件名：输出文件名，缺省为标准输出。即指定目的文件。< of=output file >

3. ibs=bytes：一次读入bytes个字节，即指定一个块大小为bytes个字节。  
   obs=bytes：一次输出bytes个字节，即指定一个块大小为bytes个字节。  
   bs=bytes：同时设置读入/输出的块大小为bytes个字节。

4. cbs=bytes：一次转换bytes个字节，即指定转换缓冲区大小。

5. skip=blocks：从输入文件开头跳过blocks个块后再开始复制。

6. seek=blocks：从输出文件开头跳过blocks个块后再开始复制。  
   注意：通常只用当输出文件是磁盘或磁带时才有效，即备份到磁盘或磁带时才有效。

7. count=blocks：仅拷贝blocks个块，块大小等于ibs指定的字节数。

8. conv=conversion：用指定的参数转换文件。    
   - ascii：转换ebcdic为ascii
   - ebcdic：转换ascii为ebcdic
   - ibm：转换ascii为alternate ebcdic  
   - block：把每一行转换为长度为cbs，不足部分用空格填充
   - unblock：使每一行的长度都为cbs，不足部分用空格填充
   - lcase：把大写字符转换为小写字符
   - ucase：把小写字符转换为大写字符
   - swab：交换输入的每对字节
   - noerror：出错时不停止
   - notrunc：不截短输出文件
   - sync：将每个输入块填充到ibs个字节，不足部分用空（NUL）字符补齐。

### 2. dd应用实例

1. 将本地的/dev/hdb整盘备份到/dev/hdd  
   #dd if=/dev/hdb of=/dev/hdd

2. 将/dev/hdb全盘数据备份到指定路径的image文件  
   #dd if=/dev/hdb of=/root/image

3. 将备份文件恢复到指定盘  
   #dd if=/root/image of=/dev/hdb

4. 备份/dev/hdb全盘数据，并利用gzip工具进行压缩，保存到指定路径  
   #dd if=/dev/hdb | gzip > /root/image.gz

5. 将压缩的备份文件恢复到指定盘  
   #gzip -dc /root/image.gz | dd of=/dev/hdb

6. 备份与恢复MBR  
   备份磁盘开始的512个字节大小的MBR信息到指定文件：  
   #dd if=/dev/hda of=/root/image count=1 bs=512  
   count=1指仅拷贝一个块；bs=512指块大小为512个字节。

   恢复：  
   #dd if=/root/image of=/dev/had  
   将备份的MBR信息写到磁盘开始部分

7. 备份软盘  

   ```
   #dd if=/dev/fd0 of=disk.img count=1 bs=1440k (即块大小为1.44M)
   ```

8. 拷贝内存内容到硬盘  

   ```
   #dd if=/dev/mem of=/root/mem.bin bs=1024 (指定块大小为1k) 
   ```

9. 拷贝光盘内容到指定文件夹，并保存为cd.iso文件  

   ```
   #dd if=/dev/cdrom(hdc) of=/root/cd.iso
   ```

10. 增加swap分区文件大小

    第一步：创建一个大小为256M的文件：

    ```
    #dd if=/dev/zero of=/swapfile bs=1024 count=262144
    ```

    第二步：把这个文件变成swap文件：

    ```
    #mkswap /swapfile
    ```

    第三步：启用这个swap文件：

    ```
    #swapon /swapfile
    ```

    第四步：编辑/etc/fstab文件，使在每次开机时自动加载swap文件：

    ```
    swapfile  swap  swap  default  0 0
    ```

11. 销毁磁盘数据

    ```
    #dd if=/dev/urandom of=/dev/hda1
    ```

    注意：利用随机的数据填充硬盘，在某些必要的场合可以用来销毁数据。

12. 测试硬盘的读写速度

    ```
    #dd if=/dev/zero bs=1024 count=1000000 of=/root/1Gb.file
    #dd if=/root/1Gb.file bs=64k | dd of=/dev/null
    ```

    通过以上两个命令输出的命令执行时间，可以计算出硬盘的读、写速度。

13. 确定硬盘的最佳块大小：

    ```
    #dd if=/dev/zero bs=1024 count=1000000 of=/root/1Gb.file
    #dd if=/dev/zero bs=2048 count=500000 of=/root/1Gb.file
    #dd if=/dev/zero bs=4096 count=250000 of=/root/1Gb.file
    #dd if=/dev/zero bs=8192 count=125000 of=/root/1Gb.file
    ```

    通过比较以上命令输出中所显示的命令执行时间，即可确定系统最佳的块大小。

14. 修复硬盘：

    ```
    #dd if=/dev/sda of=/dev/sda 或dd if=/dev/hda of=/dev/hda
    ```

    当硬盘较长时间(一年以上)放置不使用后，磁盘上会产生magnetic flux point，当磁头读到这些区域时会遇到困难，并可能导致I/O错误。当这种情况影响到硬盘的第一个扇区时，可能导致硬盘报废。上边的命令有可能使这些数 据起死回生。并且这个过程是安全、高效的。

15. 利用netcat远程备份

    ```
    #dd if=/dev/hda bs=16065b | netcat < targethost-IP > 1234
    ```

    在源主机上执行此命令备份/dev/hda

    ```
    #netcat -l -p 1234 | dd of=/dev/hdc bs=16065b
    ```

    在目的主机上执行此命令来接收数据并写入/dev/hdc

    ```
    #netcat -l -p 1234 | bzip2 > partition.img
    #netcat -l -p 1234 | gzip > partition.img
    ```

    以上两条指令是目的主机指令的变化分别采用bzip2、gzip对数据进行压缩，并将备份文件保存在当前目录。

16. 将一个很大的视频文件中的第i个字节的值改成0×41（也就是大写字母A的ASCII值）

    ```
    echo A | dd of=bigfile seek=$i bs=1 count=1 conv=notrunc
    ```

    

### 3. `/dev/null`和`/dev/zero`的区别

- `/dev/null`，外号叫无底洞，你可以向它输出任何数据，它通吃，并且不会撑着！

- `/dev/zero`，是一个输入设备，你可你用它来初始化文件。该设备无穷尽地提供0，可以使用任何你需要的数目——设备提供的要多的多。他可以用于向设备或文件写入字符串0。

`/dev/null`——它是空设备，也称为位桶（bit bucket）。任何写入它的输出都会被抛弃。如果不想让消息以标准输出显示或写入文件，那么可以将消息重定向到位桶。

```
#if=/dev/zero of=./test.txt bs=1k count=1
#ls –l

total 4
-rw-r–r–  1 oracle  dba      1024 Jul 15 16:56 test.txt

#find / -name access_log 2>/dev/null
```

#### **3.1使用/dev/null** 

把`/dev/null`看作"黑洞"， 它等价于一个只写文件，所有写入它的内容都会永远丢失.，而尝试从它那儿读取内容则什么也读不到。然而， `/dev/null`对命令行和脚本都非常的有用

-  **禁止标准输出**

  ```
  #cat $filename >/dev/null
  ```

  文件内容丢失，而不会输出到标准输出.

- **禁止标准错误**

  ```
  #rm $badname 2>/dev/null
  ```

  这样错误信息[标准错误]就被丢到太平洋去了

- **禁止标准输出和标准错误的输出**

  ```
  #cat $filename 2>/dev/null >/dev/null
  ```

  如果"$filename"不存在，将不会有任何错误信息提示；如果"$filename"存在， 文件的内容不会打印到标准输出。  
  因此，上面的代码根本不会输出任何信息。当只想测试命令的退出码而不想有任何输出时非常有用。

  ```
  #cat $filename &>/dev/null
  ```

  这样其实也可以， 由 Baris Cicek 指出

- **自动清空日志文件的内容**

  l Deleting contents of a file, but preserving the file itself, with all attendant permissions(from Example 2-1 and Example 2-3): 

  ```
  #cat /dev/null > /var/log/messages
  # : > /var/log/messages  有同样的效果， 但不会产生新的进程.（因为:是内建的）
  #cat /dev/null > /var/log/wtmp
  ```

- **隐藏cookie而不再使用**

  特别适合处理这些由商业Web站点发送的讨厌的"cookies"

  ```
  #if [ -f ~/.netscape/cookies ] # 如果存在则删除.
  #then
  #rm -f ~/.netscape/cookies
  #fi
  #ln -s /dev/null ~/.netscape/cookies
  ```

  现在所有的cookies都会丢入黑洞而不会保存在磁盘上了.

#### **3.2使用/dev/zero**

像/dev/null一样， /dev/zero也是一个伪文件， 但它实际上产生连续不断的null的流（二进制的零流，而不是ASCII型的）。 写入它的输出会丢失不见， 而从/dev/zero读出一连串的null也比较困难， 虽然这也能通过od或一个十六进制编辑器来做到。 /dev/zero主要的用处是用来创建一个指定长度用于初始化的空文件，就像临时交换文件。

- **用/dev/zero创建一个交换临时文件**

  ```bash
  #!/bin/bash
   # 创建一个交换文件.
   ROOT_UID=0 # Root 用户的 $UID 是 0.
   E_WRONG_USER=65 # 不是 root?
   FILE=/swap
   BLOCKSIZE=1024
   MINBLOCKS=40
   SUCCESS=0
   # 这个脚本必须用root来运行.
   if [ "$UID" -ne "$ROOT_UID" ]
    then
    echo; echo "You must be root to run this script."; echo
    exit $E_WRONG_USER
   fi
   blocks=${1:-$MINBLOCKS} # 如果命令行没有指定，
   #+ 则设置为默认的40块.
   # 上面这句等同如：
   # ————————————————–
   # if [ -n "$1" ]
   # then
   # blocks=$1
   # else
   # blocks=$MINBLOCKS
   # fi
   # ————————————————–
   if [ "$blocks" -lt $MINBLOCKS ]
   then
   blocks=$MINBLOCKS # 最少要有 40 个块长.
   fi
   echo "Creating swap file of size $blocks blocks (KB)."
   dd if=/dev/zero of=$FILE bs=$BLOCKSIZE count=$blocks # 把零写入文件.
   mkswap $FILE $blocks # 将此文件建为交换文件（或称交换分区）.
   swapon $FILE # 激活交换文件.
   echo "Swap file created and activated."
   exit $SUCCESS 
  ```

  关于 /dev/zero 的另一个应用是为特定的目的而用零去填充一个指定大小的文件， 如挂载一个文件系统到环回设备 （loopback device）或"安全地" 删除一个文件

- **例子创建ramdisk**

  ```bash
  #!/bin/bash
   # ramdisk.sh
   # "ramdisk"是系统RAM内存的一段，
   #+ 它可以被当成是一个文件系统来操作.
   # 它的优点是存取速度非常快 (包括读和写).
   # 缺点: 易失性, 当计算机重启或关机时会丢失数据.
   #+ 会减少系统可用的RAM.
   # 10 # 那么ramdisk有什么作用呢?
   # 保存一个较大的数据集在ramdisk, 比如一张表或字典,
   #+ 这样可以加速数据查询, 因为在内存里查找比在磁盘里查找快得多.
   E_NON_ROOT_USER=70 # 必须用root来运行.
   ROOTUSER_NAME=root
   MOUNTPT=/mnt/ramdisk
   SIZE=2000 # 2K 个块 (可以合适的做修改)
   BLOCKSIZE=1024 # 每块有1K (1024 byte) 的大小
   DEVICE=/dev/ram0 # 第一个 ram 设备
   username=`id -nu`
   if [ "$username" != "$ROOTUSER_NAME" ]
   then
    echo "Must be root to run "`basename $0`"."
    exit $E_NON_ROOT_USER
   fi
    if [ ! -d "$MOUNTPT" ] # 测试挂载点是否已经存在了,
   then #+ 如果这个脚本已经运行了好几次了就不会再建这个目录了
    mkdir $MOUNTPT #+ 因为前面已经建立了.
   fi
   dd if=/dev/zero of=$DEVICE count=$SIZE bs=$BLOCKSIZE
    # 把RAM设备的内容用零填充.                        
    # 为何需要这么做?
   mke2fs $DEVICE # 在RAM设备上创建一个ext2文件系统.
   mount $DEVICE $MOUNTPT # 挂载设备.
   chmod 777 $MOUNTPT # 使普通用户也可以存取这个ramdisk.
   # 但是, 只能由root来缷载它.
   echo ""$MOUNTPT" now available for use."
   # 现在 ramdisk 即使普通用户也可以用来存取文件了.
   # 注意, ramdisk是易失的, 所以当计算机系统重启或关机时ramdisk里的内容会消失.
   # 拷贝所有你想保存文件到一个常规的磁盘目录下.
   # 重启之后, 运行这个脚本再次建立起一个 ramdisk.
   # 仅重新加载 /mnt/ramdisk 而没有其他的步骤将不会正确工作.
   # 如果加以改进, 这个脚本可以放在 /etc/rc.d/rc.local,
   #+ 以使系统启动时能自动设立一个ramdisk.
   # 这样很合适速度要求高的数据库服务器.
   exit 0
  ```